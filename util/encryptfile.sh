#!/bin/bash
FILENAME=${1}

gpg --batch -o "${FILENAME}+gpgcrypted" -c --cipher-algo AES256 --passphrase-file ${PWD}/secretkey "${FILENAME}"